import { useEffect, useState } from 'react'
import Header from './components/Header';
import ItemCard from './components/ItemCard';
import { useSelector } from 'react-redux';
import ProductList from './components/ProductList';
import { BrowserRouter, Route, Router, Routes } from 'react-router-dom';
import Cart from './pages/Cart';
import Fav from './pages/Fav';
import { Context } from './Context';

function App() {
  
  const [showCards, setShowCards] = useState(false)
  
  return (
    <>
      <Context.Provider value={{showCards, setShowCards}}>
      <BrowserRouter>
        <Header/>
        <Routes>
          <Route path='/' element={<ProductList/>} />
          <Route path='cart' element={<Cart/>}/>
          <Route path='favourites' element = {<Fav/>}/>
        </Routes>
      </BrowserRouter>
      </Context.Provider>
    </>
  )
}

export default App
