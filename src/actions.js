export const fetchItems = () => {
    return async dispatch => {
        const response = await fetch('products.json');
        const data = await response.json();
        dispatch({ type: 'setItems', payload: data });
    };
};
