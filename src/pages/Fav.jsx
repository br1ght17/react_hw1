import { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ItemCard from "../components/ItemCard";
import Modal from "../components/modal/Modal";
import { Context } from "../Context";

function Fav(){

    let cartIds = useSelector(state=>state.fav)
    let cart = useSelector(state=>state.cart)
    let [favItems, setFavItems] = useState([])
    let dispatch = useDispatch()
    const {showCards} = useContext(Context)
    const [className, setClassName] = useState('fav')   
    const [cardClassName, setCardClassName] = useState('card')

    useEffect(()=>{
        setClassName(showCards?'fav_stretched':'fav')
        setCardClassName(showCards?'card_stretched':'card')
    },[showCards])

    useEffect(() => {
        fetch('products.json')
            .then(response => response.json())
            .then(data => {
                const filteredItems = data.filter(item => cartIds.includes(item.id));
                setFavItems(filteredItems);
            });
    }, [cartIds]);

   

    const showModal = useSelector(state=>state.modal)

    let waitlist = useSelector(state=>state.waitlist)

    let [waitItem, setWaitItem] = useState(null)

    useEffect(()=>{
      let item = favItems.filter(item=>item.id == waitlist)
      setWaitItem(item)
    }, [waitlist])

    const [productName, setProductName] = useState('');
    useEffect(() => {
        if (waitItem && waitItem.length > 0) {
            setProductName(waitItem[0].name);
        }
    }, [waitItem]);

    const closeModal = (id) => {
        dispatch({type: 'closeModal', payload: id})
      };

    function onButtonClick(type, id, dist){
        if(dist.includes(id)){
            if(type === 'favourite/addFavourite'){
                dispatch({type: 'favourite/deleteFavourite', payload: id})
            }
            else{
            console.log(`${id} item is already in list`);
            }            
        }
        else{
            dispatch({type: type,payload: id })
        }
    }

    return(
        <div className={className}>
            {
                favItems.map(item=><ItemCard 
                    id={item.id} 
                    name={item.name} 
                    price={item.price} 
                    src={item.imageUrl} 
                    sku={item.sku}
                    key={item.id}
                    onClick = {(type, id, dist)=>onButtonClick(type,id,dist)}
                    classname={cardClassName}>
                    </ItemCard>)
            }
            
            <Modal 
                    id = 'add'
                    show={showModal.add} 
                    close={() => closeModal('add')}
                    headerText={`Add product ${productName}`}
                    firstText="ADD TO CART"
                    firstClick={() => {closeModal('add'); onButtonClick('cart/addCart', waitlist, cart)}}
                /> 
        </div>
        
    )
}

export default Fav