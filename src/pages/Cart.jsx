import { useDispatch, useSelector } from "react-redux"
import ItemCard from "../components/ItemCard"
import { useEffect, useState } from "react"
import Modal from "../components/modal/Modal"
import { Form } from "formik"
import CheckoutPage from "../components/CheckoutPage"

function Cart(){

    let cartIds = useSelector(state=>state.cart)
    let [cartItems, setCartItems] = useState([])
    let items = useSelector(state=>state.items)
    let dispatch = useDispatch()

    
    useEffect(() => {
        const filteredItems = items.filter(item => cartIds.includes(item.id));
        setCartItems(filteredItems);
    }, [cartIds]);

    const showModal = useSelector(state=>state.modal)

    let waitlist = useSelector(state=>state.waitlist)

    let [waitItem, setWaitItem] = useState(null)

    useEffect(()=>{
      let item = items.filter(item=>item.id == waitlist)
      setWaitItem(item)
    }, [waitlist])

    const [productName, setProductName] = useState('');
    useEffect(() => {
        if (waitItem && waitItem.length > 0) {
            setProductName(waitItem[0].name);
        }
    }, [waitItem]);

    const closeModal = (id) => {
        dispatch({type: 'closeModal', payload: id})
      };

    return(
        <>
        <div className="cart">
        <CheckoutPage/>
            <div className="items">
                {
                    cartItems.map(item=><ItemCard 
                        id={item.id} 
                        name={item.name} 
                        price={item.price} 
                        src={item.imageUrl} 
                        sku={item.sku}
                        key={item.id}
                        added={true}
                        classname='cart_card'>
                        </ItemCard>)
                }
            </div>
        </div>
        <Modal
            id = 'delete'
            show={showModal.delete} 
            close={() => closeModal('delete')}
            headerText={`Delete product ${productName}`}
            firstText="DELETE FROM CART"
            firstClick={() => {closeModal('delete'); dispatch({type:'cart/deleteCart', payload: waitlist})}}/>
        </>
    )
}

export default Cart