


import { configureStore } from "@reduxjs/toolkit"
import {thunk} from 'redux-thunk'

const InitialState = {
    fav: JSON.parse(localStorage.getItem('fav')) || [],
    cart: JSON.parse(localStorage.getItem('cart')) || [],
    modal: {
        add: false,
        delete: false
    },
    waitlist: null,
    items: []
}


function shopReducer(state = InitialState, action){
    if(action.type === 'favourite/addFavourite'){
        localStorage.setItem('fav', JSON.stringify([...state.fav, action.payload]))
        Object.assign(document.getElementById(action.payload).querySelector('.card__fav').style, {
            color: 'white',
            backgroundColor: 'blueviolet'
        }); 
        return{
            ...state,
            fav: [...state.fav, action.payload]
        }
    }
    if(action.type === 'cart/addCart'){
        localStorage.setItem('cart', JSON.stringify([...state.cart, action.payload]))
        return{
            ...state,
            cart: [...state.cart, action.payload]
        }
    }
    if(action.type === 'cart/deleteCart'){
        const updatedCart = state.cart.filter(id => id !== action.payload);
            localStorage.setItem('cart', JSON.stringify(updatedCart));
            return {
                ...state,
                cart: updatedCart
            };
    }
    if(action.type === 'favourite/deleteFavourite'){
        const updatedFav = state.fav.filter(id => id !== action.payload)
        localStorage.setItem('fav', JSON.stringify(updatedFav))
        return{
            ...state,
            fav: updatedFav
        }
    }
    if(action.type === 'openModal'){
        document.body.style.overflow = 'hidden'
        return{
            ...state,
            modal: {...state.modal, [action.payload]: true}
        }
    }
    if(action.type === 'closeModal'){
        document.body.style.overflow = 'auto'
        return{
            ...state,
            modal: {...state.modal, [action.payload]: false}
        }
    }
    if(action.type ==='waitlist'){
        return{
            ...state,
            waitlist: action.payload
        }
    }
    if(action.type === 'setItems'){
        return {
            ...state,
            items: action.payload
        }
    }
    return state
}

const store = configureStore({
    reducer: shopReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk)
})

export default store