import { useContext } from "react"
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { Context } from "../Context"

function Header(){
    let fav = useSelector(state=>state.fav)
    let cart = useSelector(state=>state.cart)
    const {showCards, setShowCards} = useContext(Context)

    let checkboxHandler = (e) => {
        setShowCards(e.target.checked)
    }

    return(
        <header>
            <Link to='/'><h2>SHOPNAME</h2></Link>
            <label className="wraper" htmlFor="cards_show">
                <span className="label-text">Change display of items</span>
                <div className="switch-wrap">
                    <input type="checkbox" id="cards_show" onChange={checkboxHandler}/>
                    <div className="switch"></div>
                </div>
            </label>
            <div className="header__info">
                <Link to='/cart'><span>Cart: {cart.length}</span></Link>
                <br />
                <Link to='/favourites'><span>Favourites: {fav.length}</span></Link>
            </div>
        </header>
    )
}

export default Header