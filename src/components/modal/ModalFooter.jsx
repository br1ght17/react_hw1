function ModalFooter(props) {
    return (
        <div className="modal_button_wrapper">
            <button className="modal_button_success" onClick={props.firstClick}>{props.firstText}</button>
            {props.secondaryText && 
                <button className="modal_button_delete" onClick={props.secondClick}>{props.secondaryText}</button>
            }
        </div>
    );
}

export default ModalFooter