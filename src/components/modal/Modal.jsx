import ModalBody from './ModalBody';
import ModalClose from './ModalClose';
import ModalFooter from './ModalFooter';
import ModalHeader from './ModalHeader';
import ModalWrapper from './ModalWrapper';
import ModalImage from './ModalImage';
import './modal.css';

function Modal(props) {
    const stopPropagation = (e) => {
        e.stopPropagation();
    };

    return (
        <ModalWrapper show={props.show} onClose={props.close} >
            <div className="modal" onClick={stopPropagation}>
                <ModalClose onClick={props.close} />
                {props.src ? <ModalImage src={props.src} /> : null}
                <ModalHeader>{props.headerText}</ModalHeader>
                <ModalBody>{props.bodyText}</ModalBody>
                <ModalFooter 
                    firstText={props.firstText} 
                    secondaryText={props.secondaryText} 
                    firstClick={props.firstClick} 
                    secondClick={props.secondClick} 
                    
                />
            </div>
        </ModalWrapper>
    );
}

export default Modal;