import "./modal.css";

function ModalWrapper(props) {
    const handleClick = (e) => {
        if (e.target.classList.contains('modal_wrapper')) {
            props.onClose();
        }
    };

    return (
        <div className="modal_wrapper" style={{ display: props.show ? 'flex' : 'none' }} onClick={handleClick}>
            {props.children}
        </div>
    );
}

export default ModalWrapper;