


function ModalBody(props){
    return(
        <h2 className="modal_text">{props.children}</h2>
    )
}

export default ModalBody