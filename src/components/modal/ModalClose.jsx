


function ModalClose(props){
    return(
        <button className="modal_close" onClick={props.onClick}>X</button>
    )
}

export default ModalClose