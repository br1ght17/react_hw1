


function ModalHeader(props){
    return(
        <h2 className="modal_header_text">{props.children}</h2>
    )
}

export default ModalHeader