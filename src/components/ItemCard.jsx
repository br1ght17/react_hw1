import { useContext, useEffect, useState } from "react";
import Button from "./Button";
import Modal from "./modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import { Context } from "../Context";

function ItemCard({id, name, price, src, sku, added, classname }){

    let dispatch = useDispatch()
    let fav = useSelector(state=>state.fav)
    let [buttonStyles, setButtonStyles] = useState({});

   

    useEffect(() => {
        if (fav.includes(id)) {
            setButtonStyles({
                color: 'white',
                backgroundColor: 'blueviolet'
            });
        }
        else{
            setButtonStyles({
                color: 'blueviolet',
                backgroundColor: 'white'
            });
        }
    }, [fav, id]);
    

    const addToFav = () => {
        if(fav.includes(id)){
            dispatch({type: 'favourite/deleteFavourite', payload: id})
        }
        else{
            dispatch({type: 'favourite/addFavourite', payload: id})
        }
    }


    const openModal = (idModal) => {
        dispatch({type: 'openModal', payload: idModal})
        dispatch({type:'waitlist', payload: id})
    };


    return(
        <>
        <div className={classname} id={id}>
            <img src={src} alt='PRODUCT_IMAGE' title={sku} className="card__image"/>
            <h2 className="card__title">{name}</h2>
            <p className="cardd__price">{price}</p>
            {added ? <Button text='Delete from cart' onClick = {()=> openModal('delete')}/> :  <Button text = 'Add to cart' onClick={() => openModal('add')}/>}
            
            <button className="card__fav" onClick={()=>addToFav()} style = {buttonStyles}>*</button>
        </div>
      </>
    )
}

export default ItemCard