import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ItemCard from "./ItemCard";
import Modal from "./modal/Modal";
import { fetchItems } from "../actions";
import { Context } from "../Context";

function ProductList(){
    const dispatch = useDispatch();
    const items = useSelector(state => state.items);
    const cart = useSelector(state => state.cart);
    const waitlist = useSelector(state => state.waitlist);
    const showModal = useSelector(state => state.modal);

    const [waitItem, setWaitItem] = useState(null);
    const [productName, setProductName] = useState('');

    const {showCards} = useContext(Context)
    const [className, setClassName] = useState('shop')   
    const [cardClassName, setCardClassName] = useState('card')


    useEffect(()=>{
        setClassName(showCards?'shop_stretched':'shop')
        setCardClassName(showCards?'card_stretched':'card')
    },[showCards])

    useEffect(() => {
        dispatch(fetchItems());
    }, []);

    useEffect(() => {
        let item = items.filter(item => item.id === waitlist);
        setWaitItem(item);
    }, [waitlist, items]);

    useEffect(() => {
        if (waitItem && waitItem.length > 0) {
            setProductName(waitItem[0].name);
        }
    }, [waitItem]);

    function onButtonClick(type, id, dist){
        if(dist.includes(id)){
            if(type === 'favourite/addFavourite'){
                dispatch({type: 'favourite/deleteFavourite', payload: id})
            }
            else{
                console.log(`${id} item is already in list`);
            }
        }
        else{
            dispatch({type: type, payload: id });
        }
    }

    const closeModal = (id) => {
        dispatch({type: 'closeModal', payload: id});
    };

    return (
        <div className={className}>
            {
                items.map((item) => (
                    <ItemCard 
                        id={item.id} 
                        name={item.name} 
                        price={item.price} 
                        src={item.imageUrl} 
                        sku={item.sku}
                        key={item.id}
                        added={false}
                        classname={cardClassName}
                    />
                ))
            }
            <Modal 
                id='add'
                show={showModal.add} 
                close={() => closeModal('add')}
                headerText={`Add product ${productName}`}
                firstText="ADD TO CART"
                firstClick={() => {closeModal('add'); onButtonClick('cart/addCart', waitlist, cart)}}
            /> 
        </div>
    );
}

export default ProductList;
