import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { PatternFormat } from "react-number-format";
import { useSelector, useDispatch } from "react-redux";
import Button from "./Button";

const formSchema = Yup.object().shape({
  firstName: Yup.string().required("First Name is required"),
  lastName: Yup.string().required("Last Name is required"),
  age: Yup.number()
    .min(18, "You must be at least 18 years old")
    .max(120, "Invalid age")
    .required("Age is required"),
  address: Yup.string().required("Address is required"),
  phone: Yup.string()
    .required("Phone number is required")
    .matches(
      /^\+380 \(\d{2}\) \d{3}-\d{2}-\d{2}$/,
      "Phone number must be in the format +380 (##) ###-##-##"
    ),
});

function CheckoutPage() {
  const cart = useSelector((state) => state.cart);
  let dispatch = useDispatch()

  return (
    <div className="form">
      <h2 className="checkout__title">Check Out</h2>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          address: "",
          phone: "+380",
        }}
        validationSchema={formSchema}
        onSubmit={(values, { resetForm }) => {
          console.log("Form Values:", values);
          console.log("Cart Items:", cart);

          cart.forEach((item) => {
            dispatch({type: 'cart/deleteCart', payload: item})
          });

          resetForm();
        }}
      >
        {({ isSubmitting, setFieldValue }) => (
          <Form className="checkout__form">
            <div className="form_input">
              <label htmlFor="firstName">First Name</label>
              <Field
                id="firstName"
                name="firstName"
                type="text"
                autoComplete="given-name"
              />
            </div>
            <ErrorMessage name="firstName" component="div" className="error" />
            <div className="form_input">
              <label htmlFor="lastName">Last Name</label>
              <Field
                id="lastName"
                name="lastName"
                type="text"
                autoComplete="family-name"
              />
            </div>
            <ErrorMessage name="lastName" component="div" className="error" />
            <div className="form_input">
              <label htmlFor="age">Age</label>
              <Field id="age" name="age" type="number" autoComplete="age" />
            </div>
              <ErrorMessage name="age" component="div" className="error" />
            <div className="form_input">
              <label htmlFor="address">Address</label>
              <Field
                id="address"
                name="address"
                type="text"
                autoComplete="street-address"
              />
            </div>
              <ErrorMessage name="address" component="div" className="error" />
            <div className="form_input">
              <label htmlFor="phone">Phone</label>
              <PatternFormat
                format="+380 (##) ###-##-##"
                mask="_"
                id="phone"
                name="phone"
                type="tel"
                autoComplete="tel"
                placeholder="+380 (##) ###-##-##"
                customInput={Field}
                allowEmptyFormatting={true}
                onValueChange={(values) => {
                  setFieldValue("phone", values.formattedValue);
                }}
              />
            </div>
              <ErrorMessage name="phone" component="div" className="error" />
            <Button className="checkout__form-btn" type="submit" disabled={isSubmitting} text='Checkout'/>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default CheckoutPage;
